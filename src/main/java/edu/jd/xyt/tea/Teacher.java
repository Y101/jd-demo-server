package edu.jd.xyt.tea;

import java.util.Date;

public class Teacher {

    private String t_id;
    private String t_name;
    private Integer t_sex;// 1 男 2 女
    private Date t_birth;
    private Integer t_edu; //1 高中 // 2 专科 3 本科 4 硕士 5 博士
    private String t_title; //职务
    private String t_remakr;
    private String t_avatar;//头像
    private Integer t_status; // 1 在职 2 离职

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }

    public Integer getT_sex() {
        return t_sex;
    }

    public void setT_sex(Integer t_sex) {
        this.t_sex = t_sex;
    }

    public Date getT_birth() {
        return t_birth;
    }

    public void setT_birth(Date t_birth) {
        this.t_birth = t_birth;
    }

    public Integer getT_edu() {
        return t_edu;
    }

    public void setT_edu(Integer t_edu) {
        this.t_edu = t_edu;
    }

    public String getT_title() {
        return t_title;
    }

    public void setT_title(String t_title) {
        this.t_title = t_title;
    }

    public String getT_remakr() {
        return t_remakr;
    }

    public void setT_remakr(String t_remakr) {
        this.t_remakr = t_remakr;
    }

    public String getT_avatar() {
        return t_avatar;
    }

    public void setT_avatar(String t_avatar) {
        this.t_avatar = t_avatar;
    }

    public Integer getT_status() {
        return t_status;
    }

    public void setT_status(Integer t_status) {
        this.t_status = t_status;
    }
}
