package edu.jd.xyt.tea;

import edu.jd.xyt.common.Utils;
import edu.jd.xyt.tea.TeaDao;
import edu.jd.xyt.tea.Teacher;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class TeaService {
    public List<Teacher> getTeacherList() {

        SqlSession sess = Utils.openSession();
        try{
            TeaDao dao = sess.getMapper(TeaDao.class);
            List<Teacher> teaList = dao.findTeacherList();
            sess.commit();

            return teaList;
        }catch (Exception e){
            sess.rollback();
            throw new RuntimeException("查询教师信息失败！",e);

        }finally {
            sess.close();
        }


    }
}
