package edu.jd.xyt.tea;

import edu.jd.xyt.tea.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper //本注解明确说明本接口是mybatis的映射器
public interface TeaDao {
    @Select("select * from t_teacher")
    List<Teacher> findTeacherList();

}
