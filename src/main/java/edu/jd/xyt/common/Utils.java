package edu.jd.xyt.common;

import com.alibaba.fastjson.JSON;
import com.sun.net.httpserver.HttpServer;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.omg.CORBA.PUBLIC_MEMBER;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpCookie;

public class Utils {

    //工具类方法一般为静态的
    private static final String MYBATIS_CONFIG = "/mybatis.xml";
    private static final SqlSessionFactory FACTORY = buildFactory();

    private static SqlSessionFactory buildFactory() {
        try {
            InputStream in = Resources.getResourceAsStream(MYBATIS_CONFIG);

            //会话工厂
            return new SqlSessionFactoryBuilder().build(in);
        }catch (Exception e){
            throw new RuntimeException("创建SqlSessionFactory失败！",e);
        }
    }

    public static SqlSessionFactory getFactory(){
        return FACTORY;
    }

    public static SqlSession openSession(){
        return FACTORY.openSession();
    }



    /**
     *将一个Java对象转化为json串，并向浏览器输出
     * @param resp 响应对象
     * @param obj  需要转化为json串的java对象
     * @throws IOException
     */
    public static void outJson(HttpServletResponse resp,Object obj) throws IOException {
        resp.setContentType("application/json; charset=UTF-8");//设置字符编码
        PrintWriter out = resp.getWriter();//获取向客户端发送字符信息流对象

        //将LIST集合对象转化为JSON格式字符
        String jsonString = JSON.toJSONString(obj);
        out.print(jsonString);

        out.flush();
        out.close();

    }
    public static void outResult(HttpServletResponse resp,Result result) throws IOException {
        outJson(resp,result);
    }
}
