package edu.jd.xyt.common;

public class Result {


    public static Result success(){
        return new Result(200,true,null,null);
    }

    public static Result success(String message){
        return new Result(200,true,message,null);
    }

    public static Result success(String message,Object data){
        return new Result(200,true,message,data);
    }

    public static Result success(Object data){
        return new Result(200,true,null,data);
    }
    public static Result fail(int code,String message){
        return new Result(code,false,message,null);
    }

    private  int code;//200成功 500错误
    private  boolean sucesss;//是否成功
    private  String message;
    private  Object data;




    private Result(int code, boolean sucesss, String message, Object data) {
        this.code = code;
        this.sucesss = sucesss;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public boolean isSucesss() {
        return sucesss;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }
}
